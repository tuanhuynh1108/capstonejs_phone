const Base_URL = "https://63fec65ec5c800a72385839b.mockapi.io";
let DSSP = [];
function fetchSP() {
  axios({
    url: `${Base_URL}/capstone`,
    method: "GET",
  })
    .then(function (res) {
      DSSP = res.data;
      let sp = res.data.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.type,
          item.desc,
          item.img,
          item.quantity
        );
      });

      render(sp);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

fetchSP(); // ra giao diện
let gioHang = [];

// fetchGioHang
function fetchGioHang() {
  axios({
    url: `${Base_URL}/user`,
    method: "GET",
  })
    .then((res) => {
      let SPGH = res.data.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.type,
          item.desc,
          item.img,
          item.quantity
        );
      });
      renderGioHang(SPGH);
    })
    .catch((err) => {
      console.log(err);
    });
}

function chonLoaiDt() {
  let chonDT = document.getElementById("chonDT").value;

  if (chonDT === "Apple") {
    axios({
      url: `${Base_URL}/capstone`,
      method: "GET",
    })
      .then(function (res) {
        let sp = res.data.map((item) => {
          return new SanPham(
            item.id,
            item.name,
            item.price,
            item.screen,
            item.backCamera,
            item.frontCamera,
            item.type,
            item.desc,
            item.img
          );
        });
        let filter = sp.filter(function (item) {
          return item.type == "iphone" || item.type == "Iphone";
        });
        console.log("filter: ", filter);
        render(filter);
      })
      .catch(function (err) {
        console.log("err: ", err);
      });
  } else if (chonDT === "SamSung") {
    axios({
      url: `${Base_URL}/capstone`,
      method: "GET",
    })
      .then(function (res) {
        let sp = res.data.map((item) => {
          return new SanPham(
            item.id,
            item.name,
            item.price,
            item.screen,
            item.backCamera,
            item.frontCamera,
            item.type,
            item.desc,
            item.img
          );
        });
        let filter = sp.filter(function (item) {
          return item.type == "Samsung";
        });
        console.log("filter: ", filter);
        render(filter);
      })
      .catch(function (err) {
        console.log("err: ", err);
      });
  } else {
    fetchSP();
  }
}

function themSP(id) {
  // call user = gio hang
  /**
   * khi nhấn nút:
   * Phải so sánh với biến gioHang (giống với mockAPI)
   *     + nếu chu7a có thì pót, fetch lại giỏ hàng
   *     + nếu có thì put vào , quantiti++ , fetch lại giỏ hàng
   */
  console.log("DSSP sun", DSSP);
  console.log("SUn Gio Hang", gioHang);

  axios({
    url: `${Base_URL}/user`,
    method: "GET",
  })
    .then(function (res) {
      let gioHang = res.data;
      toastify("Đã thêm sản phẩm vào giỏ hàng của bạn");

      console.log("gioHang: ", gioHang);
      if (gioHang.length == 0) {
        axios({
          url: `${Base_URL}/user`,
          method: "POST",
          data: DSSP[id - 1],
        })
          .then((res) => {
            fetchGioHang();
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        // for (let i = 0; i < gioHang.length; i++) {
        //   if (DSSP[id - 1].name == gioHang[i].name) {
        //     gioHang[i].quantity++;
        //     axios({
        //       url: `${Base_URL}/user/${i + 1}`,
        //       method: "PUT",
        //       data: gioHang[i],
        //     })
        //       .then((res) => {
        //         console.log(res); // chạy dòng này
        //         fetchGioHang();
        //       })
        //       .catch((err) => {
        //         console.log(err);
        //       });

        //   } else if (DSSP[id - 1].name != gioHang[i].name) {
        //     axios({
        //       url: `${Base_URL}/user`,
        //       method: "POST",
        //       data: DSSP[id - 1],
        //     })
        //       .then((res) => {
        //         console.log("res: ", res); // chạy dòng này
        //         fetchGioHang();
        //       })
        //       .catch((err) => {
        //         console.log(err);
        //       });
        //   }
        // }
        let spCanThem = gioHang.find((item) => {
          return item.name == DSSP[id - 1].name;
        });
        console.log("spCanThem: ", spCanThem);
        if (spCanThem) {
          spCanThem.quantity++;
          axios({
            url: `${Base_URL}/user/${spCanThem.id}`,
            method: "PUT",
            data: spCanThem,
          })
            .then((res) => {
              console.log(res); // chạy dòng này
              fetchGioHang();
            })
            .catch((err) => {
              console.log(err);
            });
        } else {
          axios({
            url: `${Base_URL}/user`,
            method: "POST",
            data: DSSP[id - 1],
          })
            .then((res) => {
              console.log("res: ", res); // chạy dòng này
              fetchGioHang();
            })
            .catch((err) => {
              console.log(err);
            });
        }
      }

      // fetchGioHang();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function xoaSPGioHang(id) {
  axios({
    url: `${Base_URL}/user/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      toastify("Xóa sản phẩm thành công");
      fetchGioHang();
    })
    .catch((err) => {
      console.log(err);
    });
}

function tangSoLuong(id) {
  axios({
    url: `${Base_URL}/user/${id}`,
    method: "GET",
  })
    .then((res) => {
      let sp = res.data;
      sp.quantity++;
      axios({
        url: `${Base_URL}/user/${id}`,
        method: "PUT",
        data: sp,
      })
        .then((res) => {
          toastify("Đã tăng thêm 1 sản phẩm");
          fetchGioHang();
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}

function giamSoLuong(id) {
  axios({
    url: `${Base_URL}/user/${id}`,
    method: "GET",
  })
    .then((res) => {
      let sp = res.data;
      sp.quantity--;
      axios({
        url: `${Base_URL}/user/${id}`,
        method: "PUT",
        data: sp,
      })
        .then((res) => {
          toastify("Đã giảm 1 sản phẩm");
          fetchGioHang();
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}
function gioHangUser() {
  document.getElementById("cart").style.display = "block";
  fetchGioHang();
}
let modal = document.getElementById("cart");
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
function closeCart() {
  document.querySelector("#cart").style.display = "none";
}
function thanhToan() {
  axios({
    url: `${Base_URL}/user`,
    method: "GET",
  })
    .then((res) => {
      let gioHang = res.data;
      console.log("gioHang: ", gioHang.length);
      function deleteGioHang(length) {
        if (length == 0) {
          return fetchGioHang();
        } else {
          axios({
            url: `${Base_URL}/user/${length}`,
            method: "DELETE",
          })
            .then((res) => {
              deleteGioHang(length - 1);
              console.log(res);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      }
      deleteGioHang(gioHang.length);
    })
    .catch((err) => {
      console.log(err);
    });
}
